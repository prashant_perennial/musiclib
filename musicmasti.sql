-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 05, 2014 at 06:19 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `musicmasti`
--
CREATE DATABASE IF NOT EXISTS `musicmasti` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `musicmasti`;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(240) NOT NULL,
  `email` varchar(240) NOT NULL,
  `password` varchar(240) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(500) NOT NULL,
  `activation_code` varchar(50) NOT NULL DEFAULT '0',
  `tstamp` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `date`, `image`, `activation_code`, `tstamp`) VALUES
(3, 'asd', 'admin@gffxdg.com', 'e6e061838856bf47e1de730719fb2609', '2014-07-10 12:22:11', 'Jellyfish.jpg', '0', 0),
(4, 'admin', 'admin@gmail.com', 'admin@1234', '2014-07-10 12:25:35', 'defualt.jpg', '0', 0),
(5, 'sfgtdgfdg', 'admin@gfdg.hgj', 'e6e061838856bf47e1de730719fb2609', '2014-07-10 12:59:58', '', '0', 0),
(6, 'hgfghfg', 'admingfhgfhh@sdfgsdfg.vn', 'e6e061838856bf47e1de730719fb2609', '2014-07-10 13:21:01', '453859528b76397a5a12a9d2f36ba702', '0', 0),
(7, 'thfghfh', 'adminfd@ghfdgb.hgfh', 'e6e061838856bf47e1de730719fb2609', '2014-07-10 13:22:37', 'Jellyfish.jpg', '0', 0),
(24, 'comadmin', 'admin@gmail.in', 'admin@gmail', '2014-07-10 12:22:11', 'Penguins.jpg', '0', 0),
(8, 'prashant', 'prashan.walke@perennialsys.com', '717740cdddfb3c47557e2af1a610d689', '2014-07-11 09:05:30', 'Chrysanthemum.jpg', '0', 0),
(9, 'prashantwalke', 'prashant.walke@gmail.com', '304a4e804866aaaa65f481b4f9281337', '2014-07-11 09:08:03', 'Tulips.jpg', '0', 0),
(10, 'test', 'test@gmail.com', '780ea9376333dabb1959f16e1622e00c', '2014-07-11 11:09:07', 'Penguins.jpg', '0', 0),
(11, 'fdgdgFHGF', 'adminA@SDAS.HG', 'admin@123GFH@1', '2014-07-10 12:22:11', 'Chrysanthemum.jpg', '0', 0),
(12, 'sdsafdsafdsf', 'admin@sadfasdf.kk', 'admin@123ghfA@1', '2014-07-10 12:22:11', 'dfd', '0', 0),
(13, 'sdsafdsafdsf', 'admin@sadfasdf.kk', 'admin@123ghfA@1', '2014-07-10 12:22:11', 'dfd', '0', 0),
(14, 'sdsafdsafdsf', 'admin@sadfasdf.kk', 'admin@123ghfA@1', '2014-07-10 12:22:11', 'dfd', '0', 0),
(15, 'dfsfsdfsdf', 'admin@fgsdfggsfg.hj', 'admin@123dsf!d@s1A', '2014-07-10 12:22:11', 'dfd', '0', 0),
(17, 'test', 'test@gmail.com', '9766e9a94491cbe22f339b101ab9f631', '2014-07-10 12:22:11', 'Chrysanthemum.jpg', '0', 0),
(18, 'asas', 'sasas@asda.hf', 'a4c73760ce8fbd566bafc65b08714ecf', '2014-07-10 12:22:11', 'footerBg.jpg', '0', 0),
(19, 'dsaff', 'admin@fsf.jfg', 'a3b246231dad4c3027ab2955a021385f', '2014-07-10 12:22:11', 'activethumb.png', '0', 0),
(20, 'sfdsfdsf', 'admin@gdfghgfh.fgh', 'c08f73c2979091f88da2e96c6049144c', '2014-07-10 12:22:11', 'blockquote.png', '0', 0),
(21, 'fsadf', 'admin@gmasil.com', '856c9f34237dc7578add42af48f22098', '2014-07-10 12:22:11', 'clock-icon.png', '0', 0),
(22, 'fdsgfdsgdfg', 'admin@dfdsaf.gf', 'd65936fc527d96e16a0336ed2b7c732b', '2014-07-10 12:22:11', 'clock-icon.png', '0', 0),
(23, 'dsc', 'admincds@dfsd.jh', '2e6cd7c5c897a23d868c41c2c582f020', '2014-07-10 12:22:11', 'deafult.jpg', '0', 0),
(25, 'adamin', 'ad@gmail.com', 'a14ad3a66eb9bafc7b2e0518d6f2595c', '2014-07-10 12:22:11', 'Chrysanthemum.jpg', '0', 0),
(26, 'Prashant', 'prashant.walke@perennialsys.com', 'cd84d683cc5612c69efe115c80d0b7dc', '2014-07-10 12:22:11', 'Penguins.jpg', '958415', 1406882441);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
