<?php

class db_connection {
        private $result = array();
        private $con;
        private $numResults;
        protected $db_name = 'musicmasti';
        protected $db_user = 'root';
        protected $db_pass = '';
        protected $db_host = 'localhost'; 
        
        
        public function connect(){
                    if(!$this->con){
                            $myconn = @mysql_connect($this->db_host,$this->db_user,$this->db_pass);  
                            if($myconn){  
                                 $seldb = @mysql_select_db($this->db_name,$myconn);  
                                 if($seldb){
                                      $this->con = true;  
                                      return true;  
                                 }else{  
                                      return false;  
                                 }  
                            }else{
                                 return false;  
                            }  
                       }else{  
                            return true;  
                 }
        }  
        
        public function disconnect(){
                    if($this->con){
                        if(@mysql_close()){
                             $this->con = false;
                             return true;
                        }else{
                             return false;
                        }
                    }
        }  
        
      public function select($table, $rows = '*', $where = null, $order = null){
    	$q = 'SELECT '.$rows.' FROM '.$table;
		if($where != null){
        	$q .= ' WHERE '.$where;
		}
        if($order != null){
            $q .= ' ORDER BY '.$order;
		}
        if($this->tableExists($table)){
        	$query = mysql_query($q);
			if($query){
				$this->numResults = mysql_num_rows($query);
				for($i = 0; $i < $this->numResults; $i++){
					$r = mysql_fetch_array($query);
                	$key = array_keys($r);
                	for($x = 0; $x < count($key); $x++){
                		// Sanitizes keys so only alphavalues are allowed
                    	if(!is_int($key[$x])){
                    		if(mysql_num_rows($query) > 1){
                    			$this->result[$i][$key[$x]] = $r[$key[$x]];
							}else if(mysql_num_rows($query) < 1){
								$this->result = null;
							}else{
								$this->result[$key[$x]] = $r[$key[$x]];
							}
						}
					}
				}
				return true;
			}else{
				return false;
			}
      	}else{
      		return false;
      }
      
        }
        public function insert($table,$params=array()){
    	// Check to see if the table exists
    	 if($this->tableExists($table)){
    	 	$sql='INSERT INTO `'.$table.'` (`'.implode('`, `',array_keys($params)).'`) VALUES ("' . implode('", "', $params) . '")';
            $this->myQuery = $sql; // Pass back the SQL
            // Make the query to insert to the database
            if($ins = @mysql_query($sql)){
            	array_push($this->result,mysql_insert_id());
                return true; // The data has been inserted
            }else{
            	array_push($this->result,mysql_error());
                return false; // The data has not been inserted
            }
        }else{
        	return false; // Table does not exist
        }
    }
        public function delete($table,$where = null)
    {
        if($this->tableExists($table))
        {
            if($where == null)
            {
                $delete = 'DELETE '.$table;
            }
            else
            {
                $delete = 'DELETE FROM '.$table.' WHERE '.$where;
            }
            $del = @mysql_query($delete);
            if($del)
            {
                return true;
            }
            else
            {
               return false;
            }
        }
        else
        {
            return false;
        }
    }
        public function update($table,$rows,$where)
    {
        if($this->tableExists($table))
        {
            // Parse the where values
            // even values (including 0) contain the where rows
            // odd values contain the clauses for the row
            for($i = 0; $i < count($where); $i++)
            {
                if($i%2 != 0)
                {
                    if(is_string($where[$i]))
                    {
                        if(($i+1) != null)
                            $where[$i] = '"'.$where[$i].'" AND ';
                        else
                            $where[$i] = '"'.$where[$i].'"';
                    }
                }
            }

           // $where = implode('=',$where);
            $update = 'UPDATE '.$table.' SET ';
            $keys = array_keys($rows);
            for($i = 0; $i < count($rows); $i++)
           {
                if(is_string($rows[$keys[$i]]))
                {
                    $update .= $keys[$i].'="'.$rows[$keys[$i]].'"';
                }
                else
                {
                    $update .= $keys[$i].'='.$rows[$keys[$i]];
                }
                // Parse to add commas
                if($i != count($rows)-1)
                {
                    $update .= ',';
                }
            }
            $update .= ' WHERE '.$where;
            $query = mysql_query($update);
            if($query)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
        
 
        private function tableExists($table){
             $tablesInDb = @mysql_query('SHOW TABLES FROM '.$this->db_name.' LIKE "'.$table.'"');
             if($tablesInDb){
                  if(mysql_num_rows($tablesInDb)==1){
                       return true;
                  }else{
                       return false;
                  }
             }
        }
        // Public function to return the data to the user
    public function getResult(){
        $val = $this->result;
        $this->result = array();
        return $val;
    }

    //Pass the SQL back for debugging
    public function getSql(){
        $val = $this->myQuery;
        $this->myQuery = array();
        return $val;
    }

    //Pass the number of rows back
    public function numRows(){
        $val = $this->numResults;
        $this->numResults = array();
        return $val;
    }

    // Escape your string
    public function escapeString($data){
        return mysql_real_escape_string($data);
    }
}

?>
