<?php

   // $audioFile = "Maid with the Flaxen Hair.mp3";
    $fileName = $_GET['id'];
    // Fetch the file info.
    $filePath = '../uploads/music/' . $fileName;
   
    if(file_exists($filePath)) {
        $fileName = basename($filePath);
        $fileSize = filesize($filePath);

        // Output headers.
        header("Cache-Control: private");
        header("Content-Type: application/force-download");
        header("Content-Type: audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3");
        header("Content-Length: ".$fileSize);
//        header("Content-Disposition: attachment; filename=".$fileName);
        header('Content-Disposition: attachment; filename="' . str_replace('"', '\\"', $fileName) . '"');

        // Output file.
        readfile ($filePath);                   
        exit();
    }
    else {
        die('The provided file path is not valid.');
    }

?>