function initialize() {
var latlng = new google.maps.LatLng(18.483484, 73.857754);
var addressMarker = new google.maps.LatLng(18.483484, 73.857754);
var myOptions = {
zoom: 10,
center: latlng,
mapTypeId: google.maps.MapTypeId.ROADMAP
};
var map = new google.maps.Map(document.getElementById("map_canvas"),
myOptions);

marker = new google.maps.Marker({ map:map, position: addressMarker });
}
